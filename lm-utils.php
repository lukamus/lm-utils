<?php
/**
 * Plugin Name: Utility Functions
 * Plugin URI: https://lukamus@bitbucket.org/lukamus/lm-utils
 * Description: Utility functions that can customize a theme. REQUIRED.
 * Version: 0.1
 * License: GPL
 * Author: Luke Morton
 * Author URI: http://lukemorton.com.au
 * 
 * Copyright 2012  Luke Morton  (email: luke.morton@internode.net.au)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as 
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * Filter translations strings before display.
 * 
 * To use, add a function such as this to functions.php or a functionality 
 * plugin.
 * 
 * function lm_filter() {
 *     global $lm_filter;
 *     $lm_filter->strings( array(
 *         'Search' => 'Replace'
 *     ));
 * }
 * add_action( 'plugins_loaded', 'lm_filter' );
 *
 * @package lm_utils
 */
class LM_Translation_Filter {
    var $strings;

    /**
     * Constructor
     */
    function __construct() {
        $this->strings = array();
    }

    /**
     * Set $strings variable
     * 
     * @param   array   strings     search => replace pairs
     * @return  array               strings
     */
    function strings( $new_strings=NULL ) {
        if ( isset ( $new_strings ) ) {
            $this->strings = $new_strings;
        }
        return $this->strings;
    }

    /**
     * Filter translation strings before they're displayed.
     *
     * @param   string  translation     Current translation
     * @param   string  text            Text being translated
     * @param   string  domain          Domain for the translation
     * @return  string                  Translated string
     */
    function filter_strings( $translation, $text=null, $domain=null ) {
        if ( isset ( $text) && isset ( $domain ) ) {
            $translations = get_translations_for_domain( $domain );
            if ( isset ( $this->strings[$text] ) ) {
                return $translations->translate( $this->strings[$text] );
            }
            return $translation;
        }
    }
}

// Instanciate singleton
global $lm_filter;
$lm_filter = new LM_Translation_Filter();

// Add filter
add_filter( 'gettext', array( $lm_filter, 'filter_strings' ), 10, 4 );

